<?php

class MesasController extends AppController
{

    public $helpers = ['Html','Form','Time'];
    public $components = ['Flash','Session','Paginator'];

    //Se pueden especificar paginaciones
    public $paginate = array(
        'limit' => 5,
        'order' => array(
            'Mesa.title' => 'asc'
        )
    );
    public function index()
    {
        //La primera línea "carga" los atributos de paginación y la segunda la orden de paginar lo del modelo Mesa
        $this->Paginator->settings = $this->paginate;
        $data = $this->Paginator->paginate('Mesa');
        //Si no desea paginar, comente todo el código menos esta línea y la siguiente
        //$data=$this->Mesa->find('all'));
        $this->set('mesas',$data);
    }

    public function view($id=null)
    {
        if(!$id){
            throw new NotFoundException("Datos inválidos");        
        }
        //Consulta que encontrará la primera coincidencia que la id=a la id del método
        $mesa = $this->Mesa->find('first',['conditions'=>['Mesa.id' => $id]]);
        //$mesero = $this->Mesero->findById($id);
        if(!$mesa)
            throw new NotFoundException("No se encuentra la mesa");
        else
        //Enviamos el mesero encontrado en la vista
            $this->set('mesa',$mesa);
    }
    //Para un nuevo registro
    public function new() {
        //Aseguramos que se envíe por POST...
        if ($this->request->is('post')) 
        {
            //Si el insert funcionó:
            if($this->Mesa->save($this->request->data)) 
            {
                $this->Flash->success('Mesa creada.');
                $this->redirect(array('action' => 'index'));
            }
        }
        //Lista de meseros
        $meseros = $this->Mesa->Mesero->find('list',['fields'=>['id','nombre_mesero']]);
        $this->set('meseros',$meseros);
    }
    //Para editar un registro desde ver
    public function edit($id=null)
    {
        //Verificación que exista la id
        if (!$id)
            throw new NotFoundException(__('No existe la id...'));
        $mesa = $this->Mesa->findById($id);    
        //Si no existe el mesero su throw, sino se envía a una vista de edición, que será la misma de "nuevo registro"
        if (!$mesa)
            throw new NotFoundException(__('No se encuentra el registro...'));
        //Pasamos los datos iniciales al formulario
        //$this->set('mesero',$mesero);   
        if ($this->request->is(array('post', 'put'))) 
        {
            $this->Mesa->id = $id;
            if ($this->Mesa->save($this->request->data)) 
            {
                $this->Flash->success(__('Mesa actualizada.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('No se pudo actualizar al mesero.'));
        }
            if (!$this->request->data) 
                $this->request->data = $mesa;
               
            $meseros = $this->Mesa->Mesero->find('list',['fields'=>['id','nombre_mesero']]);
            $this->set('meseros',$meseros);
    }

    public function delete($id=null)
    {
        if (!$this->request->is(['post','DELETE'])) 
        {
            throw new MethodNotAllowedException();
        }
        //Efectuamos el borrado dentro de un if
        if ($this->Mesero->delete($id)) 
        {
            $this->Flash->success('Mesa de id: ' . $id . ' ha sido borrada.');
            $this->redirect(array('action' => 'index'));
        }
    }

    public function assign()
    {
        $meseros = $this->Mesa->Mesero->find('lista',['fields'=>['id','nombre','apellido']]);
    }



}

