<?php

class MeserosController extends AppController
{
    //Cargando Helpers, por defecto públicos
    public $helpers = ['Html','Form','Flash'];
    public $components = ['Flash'];
    //Tabla de meseros
    public function index()
    {
        //Llamando y traslanado a la vista "index.ctp" en una variable $meseros las coincidencias de la DB
        $meseros_ = $this->Mesero->find('all');
        $this->set('meseros',$meseros_);
    }

    //ver el detalle de un trabajador, en el mismo se está pasando una variable por la url
    public function ver($id=null)
    {
        if(!$id){
            throw new NotFoundException("Datos inválidos");        
        }
        //Consulta que encontrará la primera coincidencia que la id=a la id del método
        $mesero = $this->Mesero->find('first',['conditions'=>['Mesero.id' => $id]]);
        //$mesero = $this->Mesero->findById($id);
        if(!$mesero)
            throw new NotFoundException("No se encuentra el mesero");
        else
        //Enviamos el mesero encontrado en la vista
            $this->set('mesero',$mesero);
    }
    //Para un nuevo registro
    public function new() {
        //Aseguramos que se envíe por POST...
        if ($this->request->is('post')) 
        {
            //Si el insert funcionó:
            if($this->Mesero->save($this->request->data)) 
            {
                $this->Flash->success('Mesero creado.');
                $this->redirect(array('action' => 'index'));
            }
        }
    }
    //Para editar un registro desde ver
    public function edit($id=null)
    {
        //Verificación que exista la id
        if (!$id)
            throw new NotFoundException(__('No existe la id...'));
        $mesero = $this->Mesero->findById($id);    
        //Si no existe el mesero su throw, sino se envía a una vista de edición, que será la misma de "nuevo registro"
        if (!$mesero)
            throw new NotFoundException(__('No se encuentra el registro...'));
        //Pasamos los datos iniciales al formulario
        //$this->set('mesero',$mesero);   
        if ($this->request->is(array('post', 'put'))) 
        {
            $this->Mesero->id = $id;
            if ($this->Mesero->save($this->request->data)) 
            {
                $this->Flash->success(__('Mesero actualizado.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('No se pudo actualizar al mesero.'));
        }
            if (!$this->request->data) 
                $this->request->data = $mesero;
                
    }

    public function delete($id=null)
    {
        if (!$this->request->is(['post','DELETE'])) 
        {
            throw new MethodNotAllowedException();
        }
        //Efectuamos el borrado dentro de un if
        if ($this->Mesero->delete($id)) 
        {
            $this->Flash->success('Mesero de id: ' . $id . ' ha sido borrado.');
            $this->redirect(array('action' => 'index'));
        }
    }

}