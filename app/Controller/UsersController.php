<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    //Cargando Helpers, por defecto públicos
    public $helpers = ['Html','Form','Flash'];
    public $components = ['Flash', 'Paginator'];

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Session->setFlash(__('Usuario o contraseña no válidos, vuelva a intentar'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Usuario no válido'));
        }
        $this->set('user', $this->User->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Usuario guardado'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('Error,usuario no guardado, por favor intente de nuevo.')
            );
        }
    }

    public function edit($id = null) 
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Usuario no váldo'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Usuario editado exitosamente'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('Error,usuario no guardado, por favor intente de nuevo.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        // Versión menor a 2.5 usar
        // $this->request->onlyAllow('post');
        //en lugar de la siguiente línea

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Usuario no válido'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('Usuario eliminado del sistema'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Usuario no pudo ser eliminado'));
        return $this->redirect(array('action' => 'index'));
    }

    //Sólo para enlazar la vista principal
    public function principal()
    {
        return;
    }

}

