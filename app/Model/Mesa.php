<?php
class Mesa extends AppModel
{
    //Nombre de la tabla
    public $name = 'Mesa';
    
    //Validaciones en formularios, no nulos
    public $validate = [
        'serie' => ['rule' => 'notBlank'],
        'puestos' => ['rule' => 'notBlank'],
        'posicion' => ['rule' => 'notBlank']
    ];

    //Relación muchas mesas->un mesero
    public $belongsTo = [
        'Mesero' => [
            'className' => 'Mesero',
            'foreignKey' => 'mesero_id'
        ]
    ];
}