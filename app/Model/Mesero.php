<?php
class Mesero extends AppModel
{    
    //Nombre de la tabla
    public $name = 'Mesero';

     //Un virtualfields
     public $virtualFields = array('nombre_mesero' => 'CONCAT(Mesero.nombre, " ", Mesero.apellido)');

    //Validaciones en formularios, no nulos
    public $validate = [
        'rut' => ['rule' => 'notBlank'],
        'nombre' => ['rule' => 'notBlank'],
        'apellido' => ['rule' => 'notBlank'],
        'telefono' => ['rule' => 'notBlank']
    ];

    //Relación de un mesero->muchas mesas
    public $hasMany = ['Mesa'=>[
        'className' => 'Mesa',
        'foreignKey' => 'mesero_id',
        'conditions' => '',
        'order' => 'Mesa.created DESC',
        //'limit' => '5',
        'dependent' => true
    ]];
}