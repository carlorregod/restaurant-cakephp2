<!-- Menu fijado en la zona superior del formulario -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Restaurante</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Meseros
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <?php echo $this->Html->link('Lista',['controller'=>'meseros','action'=>'index'],['class'=>'dropdown-item']); ?>
        <?php echo $this->Html->link('Nuevo',['controller'=>'meseros','action'=>'new'],['class'=>'dropdown-item']); ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mesas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <?php echo $this->Html->link('Lista',['controller'=>'mesas','action'=>'index'],['class'=>'dropdown-item']); ?>
        <?php echo $this->Html->link('Nuevo',['controller'=>'mesas','action'=>'new'],['class'=>'dropdown-item']); ?>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Usuarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <?php echo $this->Html->link('Lista',['controller'=>'users','action'=>'index'],['class'=>'dropdown-item']); ?>
        <?php echo $this->Html->link('Nuevo',['controller'=>'users','action'=>'add'],['class'=>'dropdown-item']); ?>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form><br>
  </div>
</nav>