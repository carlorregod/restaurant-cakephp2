<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title> 
	
	
	<?php
		echo $this->Html->meta('icon');
		//Estilos por defecto cakephp, están alojados en app/View/webroot/css/
		//echo $this->Html->css('cake.generic');

		//Exportando bootstrap, ruta del cdn
		echo $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

</head>
<body>
	<div id="container">
		<div id="header">
		<?php if(!isset($current_user)): ?>
			<h1>Bienvenido al sistema de gestión de restaurantes en Cakephp 2</h1>
		<?php else: ?>
			<?php echo $this->element('navbar_menu'); ?>
			<br><br><br>
			<div class="row">
				<div class="col text-justify align-right">
				<h5>Bienvenido <?php echo $current_user['username']; ?></h5>
				</div>

				<div class="col text-right">
				<?php echo $this->Html->link('Desconectarse',['controller'=>'users','action'=>'logout'],['class'=>'btn btn-danger']); ?>
				</div>
			</div>

			
			
		<?endif; ?>
		</div>
		<div id="content">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<!--
		<div id="footer">
			<?php 
			// echo $this->Html->link(
			// 		$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
			// 		'https://cakephp.org/',
			// 		array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
			// 	);
			?>
			<p>
				<?php //echo $cakeVersion; ?>
			</p>
		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>

			-->
	<br>
	<p>Ejercicio creado por Carlos Orrego Díaz <br>
	( ͡° ͜ʖ ͡°)</p>
	<!-- Zona de scripts -->
	<?php
		echo $this->Html->script(
			['https://code.jquery.com/jquery-3.4.1.min.js',
			'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',
			'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js']);
	?>
</body>
</html>
