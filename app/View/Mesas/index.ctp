<h2>Listado de mesas</h2>
<div class="container">
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Serie</th>
        <th scope="col">Puesto</th>
        <th scope="col">Descripcion</th>
        <th scope="col">Mesero</th>
        <th scope="col">Ver</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($mesas as $m):?>
        <tr>
            <td><?php echo $m['Mesa']['serie']; ?></td>
            <td><?php echo $m['Mesa']['puestos']; ?></td>
            <td><?php echo $m['Mesa']['posicion']; ?></td>
            <td><?php echo $m['Mesero']['nombre'].' '.$m['Mesero']['apellido']; ?></td>
            <td><?php echo $this->Html->link('Detalle...',['controller'=>'mesas','action'=>'view',$m['Mesa']['id']]); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paging btn-group page-buttons">
    <?php
    echo $this->Paginator->prev('< ' . __d('users', 'Anterior'), array('class' => 'page-link', 'tag' => 'button'), null, array('class' => 'btn btn-default prev disabled', 'tag' => 'button'));
    echo $this->Paginator->numbers(array('separator' => '', 'class' => 'page-link', 'currentClass' => 'disabled', 'tag' => 'button'));
    echo $this->Paginator->next(__d('users', 'Siguiente') . ' >', array('class' => 'page-link', 'tag' => 'button'), null, array('class' => 'btn btn-default next disabled', 'tag' => 'button'));
    ?>
</div>
<div>
    <?php
        //El mensaje de la paginación
        echo $this->Paginator->counter(['format'=>'Página {:page} de {:pages}, mostrando {:current} registros de {:count} totales, 
        número registro inicial {:start} y final número {:end} ']);
    ?>
</div><br>
</div>
<div class="container">
    <div class="row">
    <div class="col-3">
<?php echo $this->Html->link('Nueva mesa',['controller'=>'mesas','action'=>'new'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
<br>
<?php //echo $this->Html->link('Asignar mesa a mesero',['controller'=>'mesas','action'=>'assign'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
<!--  br  -->
<?php echo $this->Html->link('Regresar',['controller'=>'users','action'=>'principal'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
</div>
    <div class="col-6"></div>
    </div>
</div>
