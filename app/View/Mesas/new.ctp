<div class="container">
<div class="row">

<div class="col-6">
<div class="users form">

<?php
echo '<h4>Añadir nueva mesa</h4>';
echo '<br>';
//Formulario para un nuevo mesero...
echo $this->Form->create('Mesa');
echo $this->Form->input('serie',['label'=>'Serie: ','type'=>'text', 'class'=>'form-control']);
echo $this->Form->input('puestos',['label'=>'Cantidad de sillas: ','type'=>'text', 'class'=>'form-control']);
echo $this->Form->input('posicion',['label'=>'Ubicación: ','type'=>'text', 'class'=>'form-control']);
echo $this->Form->input('mesero_id',['class'=>'form-control']);
echo '<br>';

echo $this->Form->end(['label'=>'Crear Nueva mesa','class'=>'btn btn-primary']);
echo '<br>';
echo $this->Html->link('Regresar...',['controller'=>'mesas','action'=>'index'],['class'=>'btn btn-success'])

?>
</div>
</div>
<div class="col-2"></div>
</div>
</div>