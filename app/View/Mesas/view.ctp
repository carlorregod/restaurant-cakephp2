<h1>Detalle de la mesa número: <?php echo $mesa['Mesa']['id']?></h1>
<br>
<div class="row">
<div class="list-group col-4">
<ul>
    <li class="list-group-item list-group-item-action list-group-item-primary">Serie: <?php echo $mesa['Mesa']['serie']?></li>
    <li class="list-group-item list-group-item-action list-group-item-primary">Cantidad de puestos: <?php echo $mesa['Mesa']['puestos']?></li>    
    <li class="list-group-item list-group-item-action list-group-item-primary">Ubicacion: <?php echo $mesa['Mesa']['posicion']?></li>
    <li class="list-group-item list-group-item-action list-group-item-primary">Fecha de creación: <?php echo $mesa['Mesa']['created']?></li><br>
    <?php echo $this->Html->link('Regresar...',['controller'=>'mesas','action'=>'index'],['class'=>'btn btn-info btn-sm btn-block'])?><br>
    <?php echo $this->Html->link('Editar...',['controller'=>'mesas','action'=>'edit',$mesa['Mesa']['id']],['class'=>'btn btn-secondary btn-sm btn-block'])?><br>
    <?php echo $this->Form->postLink('Eliminar',['controller'=>'mesas','method'=>'DELETE','action' => 'delete', $mesa['Mesa']['id']],['confirm' => '¿Seguro?','class'=>'btn btn-danger btn-sm btn-block']); ?>
    </li>
</ul>
</div>
<div class="col-4"></div>
</div>