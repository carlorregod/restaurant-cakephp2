<?php
echo '<h1>Editar mesero</h1>';

echo '<br>'; ?>
<div class="container">
    <div class="row">
    <div class="col-4">
<?php
//Antes se hacía como $this->Form->create('Mesero', ['action'=>'edit']) pero ahora se debe pasar en la url:
echo $this->Form->create('Mesero', ['url' => ['controller'=>'meseros', 'action'=>'edit']]);
echo $this->Form->input('rut',['label'=>'RUT: ','type'=>'text','class'=>'form-control']);
echo $this->Form->input('nombre',['label'=>'Nombre: ','type'=>'text','class'=>'form-control']);
echo $this->Form->input('apellido',['label'=>'Apellido: ','type'=>'text','class'=>'form-control']);
echo $this->Form->input('telefono',['label'=>'Teléfono: ','type'=>'text','class'=>'form-control']);
//Conviene que el edit tenga un input hidden de la id
echo $this->Form->input('id', array('type' => 'hidden'));
echo '<br>';
echo $this->Form->end(['label'=>'Editar','class'=>'btn btn-primary btn-sm btn-block']);
echo '<br>';
echo $this->Html->link('Regresar...',['controller'=>'meseros','action'=>'index'],['class'=>'btn btn-secondary btn-sm btn-block']);

?>

</div>
    <div class="col-6"></div>
    </div>
</div>