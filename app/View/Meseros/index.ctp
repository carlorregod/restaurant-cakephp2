<h1>Listado de meseros</h1>
<div class="container">
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">rut</th>
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th scope="col">Teléfono</th>
        <th scope="col">Ver detalle</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($meseros as $m):?>
        <tr>
            <td><?php echo $m['Mesero']['rut']; ?></td>
            <td><?php echo $m['Mesero']['nombre']; ?></td>
            <td><?php echo $m['Mesero']['apellido']; ?></td>
            <td><?php echo $m['Mesero']['telefono']; ?></td>
            <td><?php echo $this->Html->link('Detalle...',['controller'=>'meseros','action'=>'ver',$m['Mesero']['id']]); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>

<div class="container">
    <div class="row">
    <div class="col-3">
<?php echo $this->Html->link('Nuevo mesero',['controller'=>'meseros','action'=>'new'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
<br>
<?php echo $this->Html->link('Regresar',['controller'=>'users','action'=>'principal'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
</div>
    <div class="col-6"></div>
    </div>
</div>