<div class="container">
<div class="row">

<div class="col-6">
<div class="users form">
    
<?php
echo '<h4>Añadir nuevo registro</h4>';
echo '<br>';
//Formulario para un nuevo mesero...
echo $this->Form->create('Mesero');
echo $this->Form->input('rut',['label'=>'RUT: ','type'=>'text', 'class'=>'form-control']);
echo $this->Form->input('nombre',['label'=>'Nombre: ','type'=>'text','class'=>'form-control']);
echo $this->Form->input('apellido',['label'=>'Apellido: ','type'=>'text','class'=>'form-control']);
echo $this->Form->input('telefono',['label'=>'Teléfono: ','type'=>'text','class'=>'form-control']);
echo '<br>';
echo $this->Form->end(['label'=>'Nuevo mesero','class'=>'btn btn-primary']);
echo '<br>';
echo $this->Html->link('Regresar...',['controller'=>'meseros','action'=>'index'],['class'=>'btn btn-success']);

?>

</div>
</div>
<div class="col-2"></div>
</div>
</div>