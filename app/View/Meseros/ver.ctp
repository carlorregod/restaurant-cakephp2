<h1>Detalle del mesero número: <?php echo $mesero['Mesero']['id']?></h1>
<br>
<div class="row">
<div class="list-group col-4">
<ul>
    <li class="list-group-item list-group-item-action list-group-item-primary">Nombre: <?php echo $mesero['Mesero']['nombre']?></li>
    <li class="list-group-item list-group-item-action list-group-item-primary">Apellido: <?php echo $mesero['Mesero']['apellido']?></li>    
    <li class="list-group-item list-group-item-action list-group-item-primary">Teléfono: <?php echo $mesero['Mesero']['telefono']?></li>
    <li class="list-group-item list-group-item-action list-group-item-primary">Fecha de creación: <?php echo $mesero['Mesero']['created']?></li><br>
    <?php echo $this->Html->link('Regresar...',['controller'=>'meseros','action'=>'index'],['class'=>'btn btn-info btn-sm btn-block'])?><br>
    <?php echo $this->Html->link('Editar...',['controller'=>'meseros','action'=>'edit',$mesero['Mesero']['id']],['class'=>'btn btn-secondary btn-sm btn-block'])?><br>
    <?php echo $this->Form->postLink('Eliminar',['controller'=>'meseros','method'=>'DELETE','action' => 'delete', $mesero['Mesero']['id']],['confirm' => '¿Seguro?', 'class'=>'btn btn-danger btn-sm btn-block']); ?>
</ul>
</div>
<div class="col-4">

    <h4>Mesas asignadas para el mesero</h4>
    <?php if(!empty($mesero['Mesa'])): ?>
        <?php foreach($mesero['Mesa'] as $m): ?>
            <strong class="list-group-item list-group-item-action">Identificador de mesa: <?php echo $m['serie']; ?></strong>
            <p class="list-group-item list-group-item-action">Cantidad de puestos: <?php echo $m['puestos']; ?></p>
            <p class="list-group-item list-group-item-action">Ubicación: <?php echo $m['posicion']; ?></p>
            <p>**********</p>
        <?php endforeach; ?>
    <?php else: ?>
        <p class="list-group-item list-group-item-action list-group-item-warning">Mesero sin mesas asignadas</p>
    <?php endif; ?>

</div>
</div>
</div>


