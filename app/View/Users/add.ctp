<div class="container">
<div class="row">

<div class="col-6">
<div class="users form">

<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Añadiendo nuevo usuario'); ?></legend>
        <?php echo $this->Form->input('username',['label'=>'Usuario: ','type'=>'text', 'class'=>'form-control']);
        echo $this->Form->input('password',['label'=>'Contraseña: ','type'=>'password', 'class'=>'form-control']);
        echo $this->Form->input('role', array(
            'label'=>'Rol', 'options' => array('admin' => 'Administrador', 'user' => 'Usuario'),'class'=>'form-control'
        ));
        echo '<br>';
    ?>
    </fieldset>
<?php echo $this->Form->end(['label'=>__('Crear'),'class'=>'btn btn-primary']); ?>
<br>
<?php echo $this->Html->link('Regresar',['controller'=>'users','action'=>'index'],['class'=>'btn btn-success']); ?>
</div>
</div>
<div class="col-2"></div>
</div>
</div>