<?php
echo '<h1>Editar usuario</h1>';

echo '<br>'; ?>
<div class="container">
    <div class="row">
    <div class="col-4">
<?php
//Formulario de edición
echo $this->Form->create('User', ['url' => ['controller'=>'users', 'action'=>'edit']]);
echo $this->Form->input('username',['label'=>'Usuario: ','type'=>'text', 'class'=>'form-control']);
echo $this->Form->input('password',['label'=>'Contraseña: ','type'=>'password', 'class'=>'form-control']);
//Conviene que el edit tenga un input hidden de la id
echo $this->Form->input('id', array('type' => 'hidden')); ?>
<br>

<?php echo $this->Form->end(['label'=>'Editar','class'=>'btn btn-primary btn-sm btn-block']);
echo '<br>';
echo $this->Html->link('Regresar...',['controller'=>'users','action'=>'index'],['class'=>'btn btn-secondary btn-sm btn-block'])

?>

</div>
    <div class="col-6"></div>
    </div>
</div>