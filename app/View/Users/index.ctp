<h1>Listado de usuarios</h1>
<div class="container">
<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Usuario</th>
        <th scope="col">Ver</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($users as $u):?>
        <tr>
            <td><?php echo $u['User']['id']; ?></td>
            <td><?php echo $u['User']['username']; ?></td>
            <td><?php echo $this->Html->link('Detalle...',['controller'=>'users','action'=>'view',$u['User']['id']]); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<div class="container">
    <div class="row">
    <div class="col-3">
        <?php echo $this->Html->link('Nuevo usuario',['controller'=>'users','action'=>'add'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
        <br>
        <?php echo $this->Html->link('Regresar',['controller'=>'users','action'=>'principal'],['class'=>'btn btn-secondary btn-sm btn-block']); ?>
    </div>
    <div class="col-6"></div>
    </div>
</div>

