<div class="container">
<div class="row">

<div class="col-6"><div class="users form">
    <?php echo $this->Session->flash('auth'); ?>
    <?php echo $this->Form->create('User'); ?>
        <fieldset>
            <legend>
                <?php echo __('Ingrese su usuario y contraseña'); ?>
            </legend>
            <div class="form-group">
            <?php echo $this->Form->input('username',['label'=>'Usuario', 'type'=>'text','class'=>'form-control']); ?>
            </div>
            <div class="form-group">
            <?php echo $this->Form->input('password',['label'=>'Contraseña', 'type'=>'password', 'class'=>'form-control']); ?>
            </div>
        </fieldset>
    <?php echo $this->Form->end(['label'=>__('Ingresar'),'class'=>'btn btn-primary']); ?>
    </div>
    </div>
<div class="col-2">

</div>
</div>


</div>
</div>