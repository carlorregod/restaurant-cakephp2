
    <fieldset>
        <legend><?php echo __('Menú'); ?></legend>
        <div class="row">
            <div class="col">
        <?php echo $this->Html->link('Usuarios',['controller'=>'users','action' => 'index'],['class'=>'btn btn-primary btn-sm btn-block']);
            echo '<br>';
            echo $this->Html->link('Meseros',['controller'=>'meseros','action' => 'index'],['class'=>'button btn btn-primary btn-sm btn-block']); 
            echo '<br>';
            echo $this->Html->link('Mesas',['controller'=>'mesas','action' => 'index'],['class'=>'button btn btn-primary btn-sm btn-block']); 
            echo '<br>';
            echo $this->Form->postLink('Salir',['controller'=>'users','action' => 'logout'],['class'=>'btn btn-danger btn-sm btn-block']);
            ?>
            </div>
            <div class="col-8"></div>
        </div>
    </fieldset>
    