<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Añadir usuario'); ?></legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('role', [
            'options' => ['admin' => 'Admin', 'author' => 'Usuario']
        ]);
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>