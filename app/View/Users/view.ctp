<h1>Detalle del usuario número: <?php echo $user['User']['id']?></h1>
<br>
<div class="row">
<div class="list-group col-4">
<ul>
    <li class="list-group-item list-group-item-action list-group-item-primary">Usuaro: <?php echo $user['User']['username']?></li>  
    <li class="list-group-item list-group-item-action list-group-item-primary">Fecha de creación: <?php echo $user['User']['created']?></li><br>
    <?php echo $this->Html->link('Regresar...',['controller'=>'users','action'=>'index'],['class'=>'btn btn-info btn-sm btn-block']); ?><br>
    <?php echo $this->Html->link('Editar...',['controller'=>'users','action'=>'edit',$user['User']['id']],['class'=>'btn btn-secondary btn-sm btn-block'])?><br>
    <?php 
        echo $this->Form->postLink('Eliminar',['controller'=>'users','method'=>'DELETE','action' => 'delete', $user['User']['id']],['confirm' => '¿Seguro?','class'=>'btn btn-danger btn-sm btn-block']);
    ?>

</ul>
</div>
<div class="col-6"></div>
</div>
</div>