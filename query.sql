#Base de datos
CREATE DATABASE `restaurante`;
USE `restaurante`;
#Creando tabla de meseros
CREATE TABLE `meseros` (
  `id` int(11) UNSIGNED PRIMARY KEY NOT NULL,
  `rut` varchar(15) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Dos datos de ejemplo tabla meseros
INSERT INTO `meseros` (`rut`, `nombre`, `apellido`, `telefono`, `created`, `modified`) 
VALUES ('554466-1', 'Juan', 'Manueles', '34536756655', NOW(), NULL),
VALUES ('234555454-1', 'Laura', 'Prieto', '5553223', NOW(), NULL);
VALUES ('214132-1', 'Jazmin', 'Toro', '12345454', NOW(), NULL);
VALUES ('8773332443-1', 'Tomás', 'Leo', '1223556', NOW(), NULL);
VALUES ('134567832-1', 'Carlos', 'Lizana', '66655544', NOW(), NULL);

#Creando tabla de usuarios
CREATE TABLE `users` (
    `id` INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50),
    `password` VARCHAR(255),
    `role` VARCHAR(20),
    `created` DATETIME DEFAULT NULL,
    `modified` DATETIME DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#Para el login, insertando un administrador de nombre: hola y contraseña: hola
INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`) VALUES (NULL, 'hola', '$2a$10$MawEAX5nWBDXByIPn7KCIuBt5u5nM0UkXFA4RvSyNfREAzmKEMe9G', 'admin', NOW(), null) 

#Tabla de mesas
CREATE TABLE `restaurante`.`mesas` (
   `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT , 
   `serie` VARCHAR(10) NOT NULL , 
   `puestos` VARCHAR(20) NOT NULL , 
   `posicion` VARCHAR(100) NOT NULL , 
   `created` DATETIME NULL , 
   `modified` DATETIME NULL 
) ENGINE = InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `mesas` ADD `mesero_id` INT NOT NULL AFTER `modified`; #Se usará como clave foránea
#Inserciones de ejemplo, se debe tener un mesero de id=4 y 1
INSERT INTO `mesas` (`id`, `serie`, `puestos`, `posicion`, `created`, `modified`, `mesero_id`) VALUES 
(NULL, '0001', '4', 'Al noroeste del local', NOW(), NULL, '4'), 
(NULL, '0002', '6', 'Familiar. Ubicada en zona inferior derecha', NULL, NULL, '4'), 
(NULL, '0003', '2', 'Al lado de la cantina', NULL, NULL, '4'), 
(NULL, '0004', '12', 'Al centro segundo piso', NULL, NULL, '1');